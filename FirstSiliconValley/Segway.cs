﻿using System;

namespace SiliconValley
{
    public class Segway
    {
        private string name;
        private int id;

        public Segway(int id)
        {
            this.id = id;
        }

        internal void Drive()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        internal void Tern()
        {
            Console.WriteLine("Tern.");
        }
    }
}
