﻿using System;

namespace SiliconValley
{
    public class SmartWatch
    {
        private byte[] watchArray = new byte[12];
        private int id;
        private string type;
        private string country;
        private string userName;

        public SmartWatch(int id, string type, string country, string userName)
        {
            this.id = id;
            this.type = type;
            this.country = country;
            this.userName = userName;

            for (int i = 0; i < watchArray.Length; i++)
            {
                watchArray[i] = (byte)i;
            }
        }

        public void TikTak()
        {
            for (int i = 0; i < watchArray.Length; i++)
            {
                Console.WriteLine("It is {0} o'clock now.", i);
            }
        }

        protected internal void ChangeName(string username)
        {
            this.userName = username;
            Console.WriteLine("Your name {0} change to {1}", userName, username);
        }
    }
}
