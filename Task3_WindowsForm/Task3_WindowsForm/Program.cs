﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task3_WindowsForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*
            First variant:
            var file = new FileInfo(@"C:\Projects\FozzyGroupInternship\Practise\HomeTasks_Lesson12\Task3_WindowsForm\Test.txt");
            StreamWriter writer = file.CreateText();

            writer.WriteLine("Create an application to find the specified file on the disk. ");
            writer.WriteLine("Use the FileStream class and view the file in the WPF application window or WindowsForm.");

            writer.Close();
            */
            

            //Second variant:
            string text = "Create an application to find the specified file on the disk." +
                          "Use the FileStream class and view the file in the WPF application window or WindowsForm.";

            using (FileStream fstream = new FileStream(@"C:\Projects\FozzyGroupInternship\Practise\HomeTasks_Lesson12\Task3_WindowsForm\Test.txt", FileMode.OpenOrCreate))
            {
                byte[] array = Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
