### What is this repository for? ###

* Create a library with the namespace 'Silicon Valley'. In this library, describe the classes 'Segway', 'Smart watch', 'Colider'.
  When describing classes, add access modifiers internal and internally protected.
  Create a console application and use, try to use classes and their methods from the library 'Silicon Valley'.
* Create an abstract class Citizen.
  Create classes Student, Pensioner, Worker inherited from the Citizen. Create a nonparametrized collection with the following functional:
  1. Adding an item to the collection.
  2. You can only add a Citizen.
  3. When added, the item is added to the end of the collection.If a pensioner,
  then in beginning with the previous Pensioners.
  4. The number in the queue is returned.
  5. When adding the same person (checking for equality by number passports, you need to override the Equals method and / or equality operators
  for comparing objects by passport number) the element is not added, it is issued message.
  6. Delete - from the beginning of the collection.
  7. It is possible to delete with the Citizen type of argument.
  8. The Contains method returns true / false when the item is missing / unmissing collections and number in the queue.
  9. The ReturnLast method returns the last person in the queue and its number in the queue.
  10. The Clear method clears the collection.
  11. With the collection, you can work as a foreach statement
