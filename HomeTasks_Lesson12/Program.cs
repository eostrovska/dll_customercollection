﻿using SiliconValley;
using System;

namespace Task1_DLL
{
    class Program
    {
        static void Main(string[] args)
        {
            //Colider firstColider = new Colider(); //not available
            Segway firstSegwey = new Segway(456);
            SmartWatch smartWatchApple = new SmartWatch(567, "Small", "USA", "Kate");

            Console.WriteLine("Create a library with the namespace 'Silicon Valley'. In this library, describe the classes 'Segway', 'Smart watch', 'Colider'." +
                              "When describing classes, add access modifiers internal and internally protected." +
                              "Create a console application and use, try to use classes and their methods from the library 'Silicon Valley'.");
            Console.WriteLine(new string('-', 60));

            //firstSegwey.Drive(); //not available
            //firstSegwey.Tern(); //not available
            smartWatchApple.TikTak();
            //smartWatchApple.ChangeName("Filip"); //not available

            Console.ReadKey();
        }
    }
}
