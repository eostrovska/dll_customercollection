﻿using System;

namespace Task2_CustomerCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            Pensioner pensioner1 = new Pensioner("Viktor", "Micro", "TR567", 65, 120);
            Pensioner pensioner2 = new Pensioner("Inga", "Volf", "YU786", 78, 45);
            Pensioner pensioner3 = new Pensioner("Inga", "Volf", "YU756", 78, 45);//change
            Worker worker1 = new Worker("Maks", "Lift", "rt67675", 34, "Microsoft");
            Worker worker2 = new Worker("Lisa", "Mir", "YU7890", 38, "Avon");
            Student student1 = new Student("Kate", "Mirror", "DFG678", 19, 2);
            Student student2 = new Student("Helga", "Noss", "GHJ89987", 20, 3);
            UserCollection people = new UserCollection();

            Console.WriteLine("Create an abstract class Citizen." +
                              "Create classes Student, Pensioner, Worker inherited from the Citizen. Create a nonparametrized collection with the following functional:" +
                              "1. Adding an item to the collection." +
                              "2. You can only add a Citizen." +
                              "3. When added, the item is added to the end of the collection.If a pensioner," +
                              "then in beginning with the previous Pensioners." +
                              "4. The number in the queue is returned." +
                              "5. When adding the same person (checking for equality by number passports, you need to override the Equals method and / or equality operators" +
                              "for comparing objects by passport number) the element is not added, it is issued message." +
                              "6. Delete - from the beginning of the collection." +
                              "7. It is possible to delete with the Citizen type of argument." +
                              "8. The Contains method returns true / false when the item is missing / unmissing collections and number in the queue." +
                              "9. The ReturnLast method returns the last person in the queue and its number in the queue." +
                              "10. The Clear method clears the collection." +
                              "11. With the collection, you can work as a foreach statement");
            Console.WriteLine(new string('-', 60));

            people.Add(pensioner1);
            people.Add(pensioner2);
            people.Add(worker1);
            people.Add(worker2);

            try
            {
                people.Add(pensioner3);
            }
            catch (AddingSameElements message)
            {
                Console.WriteLine(message);
            }

            Console.WriteLine(people.Equals(student1));
            //people.RemoveAt(1);
            //people.RemoveAtTheBeggining();
            //people.Remove(pensioner3);
            //people.Contains(student2);
            people.ReturnLast();

            foreach (var element in people)
                Console.WriteLine(element);

            Console.ReadKey();
        }
    }
}
