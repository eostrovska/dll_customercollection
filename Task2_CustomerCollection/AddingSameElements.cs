﻿using System;

namespace Task2_CustomerCollection
{
    class AddingSameElements : Exception
    {
        public AddingSameElements(string aMessage)
            : base(aMessage) { }
    }
}
