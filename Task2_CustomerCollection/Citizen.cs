﻿using System;

namespace Task2_CustomerCollection
{
    abstract class Citizen
    {
        string name;
        string lastName;
        string passportNumber;
        byte age;

        public string PassportNumber { set { passportNumber = value; } get { return passportNumber; } }

        public Citizen() { }

        public Citizen(string name, string lastName, string passportNumber, byte age)
        {
            this.name = name;
            this.lastName = lastName;
            this.passportNumber = passportNumber;
            this.age = age;
        }
        abstract public void Walk();
        abstract public void Speek();

        public override bool Equals(Object citizen)
        {
            if (citizen == null || this.GetType() != citizen.GetType())
            {
                return false;
            }

            Citizen equalingCitizen = (Citizen)citizen;
            return (passportNumber == equalingCitizen.passportNumber);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
