﻿using System;

namespace Task2_CustomerCollection
{
    class Worker : Citizen
    {
        string company;

        public Worker(string name, string lastName, string passportNumber, byte age, string company)
            : base(name, lastName, passportNumber, age)
        {
            this.company = company;
        }

        public override void Speek()
        {
            Console.WriteLine("Worker hates to speek.");
        }

        public override void Walk()
        {
            Console.WriteLine("Worker uses bus or train instead of walking.");
        }

        public void Work()
        {
            if (company != null)
            {
                Console.WriteLine("Worker does job good in the {0} company.", company);
            }
        }
    }
}
