﻿using System;
using System.Collections;
using System.Linq;

namespace Task2_CustomerCollection
{
    class UserCollection : IEnumerable, IEnumerator
    {
        Citizen[] elements = new Citizen[0];
        int position = -1;
        private int count;

        public Citizen this[int index]
        {
            get { return elements[index]; }
            set { elements[index] = value; }
        }

        public int Add(Citizen item)
        {
            if (elements.Contains(item))
            {
                throw new AddingSameElements("Adding the same elements isn't allowed!");
            }

            var newArray = new Citizen[elements.Length + 1];
            elements.CopyTo(newArray, 0);
            if (item is Pensioner)
            {
                newArray[0] = item;
                elements = newArray;
            }
            else
            {
                newArray[newArray.Length - 1] = item;
                elements = newArray;
            }
            count++;
            return (count - 1);
        }

        public void RemoveAt(int index)
        {
            if ((index >= 0) && (index < elements.Length))
            {
                for (int i = index; i < elements.Length - 1; i++)
                    elements[i] = elements[i + 1];
            }
            count--;
        }

        public void Remove(Citizen value)
        {
            RemoveAt(IndexOf(value));
        }

        private int IndexOf(Citizen value)
        {
            for (int i = 0; i < elements.Length; i++)
                if (elements[i] == value)
                    return i;
            return -1;
        }

        public void RemoveAtTheBeggining()
        {
            RemoveAt(0);
        }

        public bool Contains(Citizen item)
        {
            foreach (var element in elements)
            {
                if (element.Equals(item))
                    return true;
            }

            return false;
        }

        public Human<Citizen, int> ReturnLast()
        {
            Human<Citizen, int> result = new Human<Citizen, int>();
            int numberOfArray = elements.Length;

            foreach (Citizen element in elements)
                result = new Human<Citizen, int>(element, numberOfArray);
            return result;
        }

        public void Clear()
        {
            elements = new Citizen[0];
        }


        bool IEnumerator.MoveNext()
        {
            if (position < elements.Length - 1)
            {
                position++;
                return true;
            }
            return false;
        }

        void IEnumerator.Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get { return elements[position]; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)this;
        }


    }
}
