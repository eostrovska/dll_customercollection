﻿using System;

namespace Task2_CustomerCollection
{
    class Student : Citizen
    {
        byte course;

        public Student(string name, string lastName, string passportNumber, byte age, byte course)
            : base(name, lastName, passportNumber, age)
        {
            this.course = course;
        }

        public override void Speek()
        {
            Console.WriteLine("Student speeks very quickly.");
        }

        public override void Walk()
        {
            Console.WriteLine("Student runs.");
        }

        public bool DoHomeTasks(byte tasksAmount, byte tasksAmountDone)
        {
            if (tasksAmount == tasksAmountDone)
                return true;
            return false;
        }
    }
}
