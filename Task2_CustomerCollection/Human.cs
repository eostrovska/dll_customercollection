﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_CustomerCollection
{
    public class Human<Citizen, Int>
    {
        public readonly Citizen citizen;
        public readonly int numberOfArray;

        public Human() { }

        public Human(Citizen citizen, int numberOfArray)
        {
            this.citizen = citizen;
            this.numberOfArray = numberOfArray;
        }
    }
}
