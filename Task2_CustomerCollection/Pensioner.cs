﻿using System;

namespace Task2_CustomerCollection
{
    class Pensioner : Citizen
    {
        byte bookAmount;

        public Pensioner(string name, string lastName, string passportNumber, byte age, byte bookAmount)
            : base(name, lastName, passportNumber, age)
        {
            this.bookAmount = bookAmount;
        }

        public override void Speek()
        {
            Console.WriteLine("Pensioner speeks a lot.");
        }

        public override void Walk()
        {
            Console.WriteLine("Pensioner walks with the stick.");
        }

        public void ReadBooks(byte bookAmount)
        {
            Console.WriteLine("Pensioner has read {} books.", bookAmount);
        }
    }
}
